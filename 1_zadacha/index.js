document.addEventListener('DOMContentLoaded', () => {
  let form = document.querySelector('.js-form'),
    formInputs = document.querySelectorAll('.js-input'),
    inputEmail = document.querySelector('.js-input-email'),
    inputPhone = document.querySelector('.js-input-phone'),
    inputCheckbox = document.querySelector('.js-input-checkbox'),
    errorMess = document.querySelector('.error_mess'),
    inputFile = document.querySelector('.fileInput')

  let fileList = null

  inputFile.addEventListener('change', (event) => {
    fileList = event.target.files
  })

  function validateEmail(email) {
    let re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,})+$/
    return re.test(String(email).toLowerCase())
  }

  function validatePhone(phone) {
    let re = /^[0-9.-\s]*$/
    return re.test(String(phone))
  }

  form.onsubmit = function (e) {
    let emailVal = inputEmail.value,
      phoneVal = inputPhone.value,
      emptyInputs = Array.from(formInputs).filter((input) => input.value === '')

    formInputs.forEach(function (input) {
      if (input.value === '') {
        input.classList.add('error')
      } else {
        input.classList.remove('error')
      }
    })

    if (emptyInputs.length !== 0) {
      errorMess.textContent = 'inputs not filled'
      return false
    }

    if (!validateEmail(emailVal)) {
      errorMess.textContent = 'email not valid'
      inputEmail.classList.add('error')
      return false
    } else {
      inputEmail.classList.remove('error')
    }

    if (!validatePhone(phoneVal)) {
      errorMess.textContent = 'phone not valid'
      inputPhone.classList.add('error')
      return false
    } else {
      inputPhone.classList.remove('error')
    }

    if (!inputCheckbox.checked) {
      errorMess.textContent = 'checkbox not checked'
      inputCheckbox.classList.add('error')
      return false
    } else {
      inputCheckbox.classList.remove('error')
    }

    if (fileList == null) {
      errorMess.textContent = 'file is not added'
      return false
    } else {
      errorMess.textContent = ''
    }
  }
})
