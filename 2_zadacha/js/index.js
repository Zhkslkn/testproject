document.addEventListener('DOMContentLoaded', () => {
  let form1 = document.querySelector('.js-form1'),
    form2 = document.querySelector('.js-form2'),
    form3 = document.querySelector('.form3'),
    form4 = document.querySelector('.form4'),
    formInputs = document.querySelectorAll('.js-input'),
    formInputs2 = document.querySelectorAll('.js-input2'),
    formInputs4 = document.querySelectorAll('.js-input4'),
    inputEmail = document.querySelector('.js-input-email4'),
    inputIIN = document.querySelector('.js-input-iin'),
    inputBIN = document.querySelector('.js-input-bin'),
    inputPhone = document.querySelector('.js-input-phone'),
    inputCheckbox = document.querySelector('.js-input-checkbox'),
    errorMess = document.querySelector('.error_mess'),
    errorMess2 = document.querySelector('.error_mess2'),
    inputFile = document.querySelector('.fileInput')

  //   let myModal = new bootstrap.Modal(document.getElementById('ModalToggle'), {})

  const privacyCheck1 = document.querySelector('#privacy_check1')
  const privacyCheck2 = document.querySelector('#privacy_check2')
  const displayBlockPhysic = document.querySelector('.displayBlockPhysic')
  const displayBlockYouridic = document.querySelector('.displayBlockYouridic')

  let fileList = null
  let arrayData = []

  inputFile.addEventListener('change', (event) => {
    fileList = event.target.files
  })

  function validateEmail(email) {
    let re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,})+$/
    return re.test(String(email).toLowerCase())
  }

  function validatePhone(phone) {
    let re = /^[0-9.-\s]*$/
    return re.test(String(phone))
  }

  function validateIIN(number) {
    let re = /^([0-9\s]{11,12})*$/
    return re.test(String(number))
  }

  let tabNav = document.querySelectorAll('.tabs-nav__item'),
    tabContent = document.querySelectorAll('.tab'),
    tabName

  function selectTabNav(first, index) {
    tabNav[first].classList.remove('is-active')
    tabNav[index].classList.add('is-active')

    tabName = tabNav[index].getAttribute('data-tab-name')
    selectTabContent(tabName)
  }

  function selectTabContent(tabName) {
    tabContent.forEach((item) => {
      item.classList.contains(tabName)
        ? item.classList.add('is-active')
        : item.classList.remove('is-active')
    })
  }

  privacyCheck1.addEventListener('click', () => {
    displayBlockPhysic.style.display = 'block'
    displayBlockYouridic.style.display = 'none'
  })

  privacyCheck2.addEventListener('click', () => {
    displayBlockYouridic.style.display = 'block'
    displayBlockPhysic.style.display = 'none'
  })

  form1.onsubmit = function (e) {
    e.preventDefault()
    let iinVal = inputIIN.value,
      emptyInputs = Array.from(formInputs).filter((input) => input.value === '')

    formInputs.forEach(function (input) {
      if (input.value === '') {
        input.classList.add('error')
      } else {
        input.classList.remove('error')
        arrayData.push(input.value)
      }
    })

    if (emptyInputs.length !== 0) {
      errorMess.textContent = 'inputs not filled'
      return false
    }

    if (!validateIIN(iinVal)) {
      errorMess.textContent = 'IIN not valid'
      inputIIN.classList.add('error')
      return false
    } else {
      inputIIN.classList.remove('error')
    }

    selectTabNav(0, 1)
  }

  form2.onsubmit = function (e) {
    e.preventDefault()
    let binVal = inputBIN.value,
      emptyInputs = Array.from(formInputs2).filter(
        (input) => input.value === ''
      )

    formInputs2.forEach(function (input) {
      if (input.value === '') {
        input.classList.add('error')
      } else {
        input.classList.remove('error')
        arrayData.push(input.value)
      }
    })

    if (emptyInputs.length !== 0) {
      errorMess.textContent = 'inputs not filled'
      return false
    }

    if (!validateIIN(binVal)) {
      errorMess.textContent = 'BIN not valid'
      inputBIN.classList.add('error')
      return false
    } else {
      inputBIN.classList.remove('error')
    }

    console.log(arrayData)
    selectTabNav(0, 1)
  }

  form3.onsubmit = function (e) {
    e.preventDefault()
    if (fileList) {
      selectTabNav(1, 2)
    }
  }

  form4.onsubmit = function (e) {
    e.preventDefault()
    let email = inputEmail.value,
      emptyInputs = Array.from(formInputs4).filter(
        (input) => input.value === ''
      )

    formInputs4.forEach(function (input) {
      if (input.value === '') {
        input.classList.add('error')
      } else {
        input.classList.remove('error')
        arrayData.push(input.value)
      }
    })

    if (emptyInputs.length !== 0) {
      errorMess.textContent = 'inputs not filled'
      return false
    }

    if (!validateEmail(email)) {
      errorMess2.textContent = 'Email not valid'
      inputEmail.classList.add('error')
      return false
    } else {
      inputEmail.classList.remove('error')
    }

    Swal.fire('Поздравляем!', 'Вы успешно заполнили все данные!', 'success')
    // myModal.show()
  }
})
